/*
  This class was written by Ben Zepp July 26th, 2017
  to solve the programming problem "Beekeeper" from Kattis.com:

  https://open.kattis.com/problems/beekeeper

  This is a verified correct solution by the Kattis judge, but includes extra
  output for tests with the -t flag for personal use. (The judge should not
  activate this option).
*/
import java.util.*;

public class Beekeeper{

  public static void solve(){
    Scanner scan = new Scanner(System.in);
    int size = scan.nextInt();
    String[] words;
    while(size > 0){
      words = new String[size];

      for(int i = 0; i < size; i++){
        words[i] = scan.next();
      }
      System.out.println(findStringWithMostVowelPairings(words));
      size = scan.nextInt();
    }
  }

  /*
  Takes in an array of Strings and returns a String with the most number of
  vowel Pairs in the String. We assume that there is only one unique String that
  satisfies this condition in the given String array.
  @param words: the Array of Strings to compare.
  */
  public static String findStringWithMostVowelPairings(String[] words){
    int mostPairsSoFar = 0;
    // Keep track of where in the array the current best soultion is, instead of
    // Storing up to 80 char long strings for every next best solution.
    int indexWithMostVowelPairs = 0;
    int numPairs = 0;
    for(int i = 0; i < words.length; i++){
      numPairs = findNumVowelPairs(words[i]);
      if(numPairs > mostPairsSoFar){
        mostPairsSoFar = numPairs;
        indexWithMostVowelPairs = i;
      }
    }
    return words[indexWithMostVowelPairs];
  }

  /*
  Finds the number of vowel pairs in a single String.
  @param word: the String to be searched.
  */
  public static int findNumVowelPairs(String word){

    int numPairsSoFar = 0;
    char currentChar;
    char lastChar;
    // Loop through a word by each character and count the number of vowel pairs
    for(int i = 1; i < word.length(); i++){
      currentChar = word.charAt(i);
      lastChar = word.charAt(i-1);
      // if the last character is a vowel and the current character is the same.
      // remember: this simple expression works because a word will never contain
      //          words with 3 vowels togeth such as "aaa" or "jeeep" as noted in
      //          the problem description.
      if(isVowel(lastChar) && currentChar == lastChar){
        numPairsSoFar += 1;
      }
    }
    return numPairsSoFar;
  }

  /*
  Returns true if @param c is a vowel. c is guarenteed to be not-null and
  a lowercase letter in the range [a-z].
  */
  public static boolean isVowel(char c){
    boolean result = false;
    switch (c){
      case 'a': result = true;
      case 'e': result = true;
      case 'i': result = true;
      case 'o': result = true;
      case 'u': result = true;
      case 'y': result = true;
        break;
      default: result = false;
        break;
      }

    return result;
  }


  /*
    Test functions for unit tests.
    Tests should probably be encompassed in their own class, but for a small
      problem solution it's irrelevant.
  */
  public void test(){
    testFindStringWithMostVowelPairings();
    testFindNumVowelPairs();
    testIsVowel();
  }

  public boolean assertEquals(int a, int b){
    return a == b;
  }

  public boolean assertEquals(boolean a, boolean b){
    return a == b;
  }

  public boolean assertEquals(String a, String b){
    return a.equals(b);
  }

  public void testIsVowel(){
    char[] tests = {'a','e','i','o','u','y',
                    'b','z' };
    Boolean[] expects = {true,true,true,true,true,true,
                         false,false};
    System.out.println("Tests for \"isVowel\"");
    for(int i = 0; i < tests.length; i++){
      boolean check = isVowel(tests[i]);
      if(assertEquals(check, expects[i])){
        System.out.println("Test case " + (i+1) +  "\nPassed:\nExpected: " + expects[i] + "\nActual:   " + check);
      }
      else{
        System.out.println("Test case " + (i+1) +  " \nFAILED:\nExpected: " + expects[i] + "\nActual:   " + check);
      }
    }
    System.out.println();
  }

  public void testFindNumVowelPairs(){
    String[] tests = {"a","aa","aaee",
                      "yeehaaw","aabbeez","beekeeper",
                      "engineer","bookkatt","letstryalongerstring",
                      "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzab"};
    int[] expects = { 0,1,2,
                      2,2,2,
                      1,1,0,
                      0 };
    System.out.println("Tests for \"findNumVowelPairs\"");
    for(int i = 0; i < tests.length; i++){
      int check = findNumVowelPairs(tests[i]);
      if(assertEquals(check, expects[i])){
        System.out.println("Test case " + (i+1) +  "\nPassed:\nExpected: " + expects[i] + "\nActual:   " + check);
      }
      else{
        System.out.println("Test case " + (i+1) +  "\nFAILED:\nExpected: " + expects[i] + "\nActual:   " + check);
      }
    }
    System.out.println();
  }

  public void testFindStringWithMostVowelPairings(){
    String[][] tests = {  {"a","aa"},
                        {"aa", "aae", "beekeeper"},
                        {"z"},
                        {"zzaabb"},
                        {"bookkatt", "eengineer"},
                        {"engineer","abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzab"} };

    String[] expects = { "aa",
                         "beekeeper",
                         "z",
                         "zzaabb",
                         "eengineer",
                         "engineer" };
    System.out.println("Tests for \"findNumVowelPairs\"");
    for(int i = 0; i < tests.length; i++){
      String check = findStringWithMostVowelPairings(tests[i]);
      if(assertEquals(check , expects[i])){
        System.out.println("Test case " + (i+1) +  "\nPassed:\nExpected: " + expects[i] + "\nActual:   " + check);
      }
      else{
        System.out.println("Test case " + (i+1) +  "\nFAILED:\nExpected: " + expects[i] + "\nActual:   " + check);
      }
    }
    System.out.println();
  }

}
