run: Beekeeper.class Driver.class
	java Driver

test: Beekeeper.class Driver.class
	java Driver -t

Beekeeper.class: Beekeeper.java
	javac Beekeeper.java

Driver.class: Driver.java
	javac Driver.java

clean: 
	rm -f *.class
