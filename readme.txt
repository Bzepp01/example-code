This repository contains the solution to the "Beekeeper" problem on
Kattis.com.  For information in the problem please refer to:

https://open.kattis.com/problems/beekeeper

Solution created by Benjamin Zepp on 07/25/17.

To run this program, simply type 'make run' from the command prompt.
If the program is being run from a system not using the Unix command line,
refer to the commands in the makefile for compiling and running the program.

To run this program along with the unit tests, type 'make test' from the
command line

To force recompilation type 'make clean' to remove all .class files.
